#include <iostream>
#include "StockTracker.h"


//includes skeletal code provided by Dr Jesse Hartloff to outline the project


StockTracker::StockTracker() { }



/*** part 1 ***/

// Registers a stock by it's ticker symbol and gives the number of shares outstanding for that stock.
// Shares outstanding is the total number of shares that exist for the stock. Your implementation should store this
// information in a data structure of your choice. Since this is a set/map homework those are the intended
// structures, but you are free to use any data structure you'd like.
//void registerTicker(string tickerSymbol, int sharesOutstanding);
void StockTracker::registerTicker(string tickerSymbol, int sharesOutstanding) {
    vector<double> values(2);
    values[0] = (double) sharesOutstanding;
    values[1] = 0.0;
    tracker[tickerSymbol] = values;
//    finder.insert(std::pair<int, string>(tickerSymbol[0]*tickerSymbol[1], dummy));
}


// Returns true if tickerSymbol has been registered, false otherwise
bool StockTracker::isTicker(string tickerSymbol) {
    return tracker.count(tickerSymbol) > 0;
}


// returns the number of shares outstanding for the stock corresponding to tickerSymbol. If the symbol has not
// been registered, return 0.
int StockTracker::getSharesOutstanding(string tickerSymbol) {
    if (isTicker(tickerSymbol)) {
        double number = tracker[tickerSymbol][0];
        return (int) number;
    }
    return 0;
}


// Updates the current price of a share of stock corresponding to tickerSymbol. This information must be stored in
// a data structure of your choosing. If the stock price was previously set, it must be updated. This function will
// be called multiple times with the same tickerSymbol.
void StockTracker::updateCurrentPrice(string tickerSymbol, double price) {
    if (getCurrentPrice(tickerSymbol) != 0.0) {
        double prev = tracker[tickerSymbol][1]*tracker[tickerSymbol][0];
        tracker[tickerSymbol][1] = price;
        std::multimap<double, string>::iterator it;
        for (it = finder.equal_range(prev).first; it != finder.equal_range(prev).second; it++) {
            if (it->second == tickerSymbol) {
                finder.erase(it);
                finder.insert(std::pair<double, string>(tracker[tickerSymbol][1] * tracker[tickerSymbol][0],
                                                        tickerSymbol));
                break;
            }
        }
    }
    else {
        tracker[tickerSymbol][1] = price;
        finder.insert(std::pair<double, string>(tracker[tickerSymbol][1] * tracker[tickerSymbol][0],
                                                tickerSymbol));
    }
}

// Return the current price of the given stock. If the stock has not been registered or the price was
// never set, return 0.0.
double StockTracker::getCurrentPrice(string tickerSymbol) {
    if (isTicker(tickerSymbol)) {
        return tracker[tickerSymbol][1];
    }
    return 0.0;
}


// Return the current market capitalization for the given tickerSymbol. Market capitalization is the current market
// value of the company and can be computed by multiplying its price by its number of shares outstanding. If the
// stock has not been registered or the price was never set, return 0.0.
double StockTracker::getMarketCap(string tickerSymbol) {
    if (isTicker(tickerSymbol)) {
        return tracker[tickerSymbol][0] * tracker[tickerSymbol][1];

    }
    return 0.0;

}


vector<string> StockTracker::topMarketCaps(int k) {
    vector<string> returner;
    int number = 0;
    std::multimap<double, string>::reverse_iterator rt;
    for (rt = finder.rbegin(); rt != finder.rend(); rt++) {

        returner.push_back(rt->second);
        number++;
        if (number == k) { break; }
    }
    return returner;

}


